var cartePokemon = {
  props:['pokemon', 'img'],
  template:
  `
  <v-card id="card" elevation="10" class="ma-2 pa-0 col-lg-4 col-md-6 col-xs-12 d-flex flex-column" max-width="32%">
        <v-card-title  :class="'d-flex flex-row '+pokemon.types[0].couleur">
            <v-chip text-color="white" color="#000000"> {{pokemon.id}}</v-chip>
            <v-container class="d-flex col-9">{{pokemon.nom}}</v-container>
          </v-card-title>
          <v-card-text>
            <v-row class="d-flex flex-row">
                <v-container class="col-6">
                  <v-container class="mt-10 d-flex flex-column">
                  <v-img class="center" :src=img max-width="200" max-height="200"></v-img>
                    <v-row class="d-flex flex-row col-12 justify-space-around">
                      <v-sheet v-for="elt in pokemon.types" :color="elt.couleur" class="pa-3 mt-4 rounded-pill">{{elt.nom}}</v-sheet>
                    </v-row>
                  </v-container>
                </v-container>
                <v-container class="d-flex flex-column col-6">
                  <v-container class="d-flex">{{pokemon.description}}</v-container>
                  <v-simple-table class="d-flex">
                    <template>
                      <tbody>
                        <tr>
                          <td>HP</td>
                          <td>{{pokemon["base"]["HP"]}}</td>
                        </tr>
                        <tr>
                          <td>Attaque</td>
                          <td>{{pokemon["base"]["Attack"]}}</td>
                        </tr>
                        <tr>
                          <td>Défense</td>
                          <td>{{pokemon["base"]["Defense"]}}</td>
                        </tr>
                        <tr>
                          <td>Attaque spéciale</td>
                          <td>{{pokemon["base"]["SpAttack"]}}</td>
                        </tr>
                        <tr>
                          <td>Défense spéciale</td>
                          <td>{{pokemon["base"]["SpDefense"]}}</td>
                        </tr>
                        <tr>
                          <td>Vitesse</td>
                          <td>{{pokemon["base"]["Speed"]}}</td>
                        </tr>
                      </tbody>
                    </template>
                  </v-simple-table>
                </v-container>
            </v-row>
          </v-card-text>
      </v-card>
  `
};