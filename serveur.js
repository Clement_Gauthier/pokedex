var express = require('express');
var serve_static = require('serve-static');
var http = require('http');
var fs = require('fs');

const app = express();
app.set('view-engine', 'ejs');

const bodyParser = require('body-parser'); // middleware
app.use(serve_static(__dirname + "/public"));
app.use(bodyParser.urlencoded({ extended: false }));

// Include Express Validator Functions
const { check, validationResult } = require('express-validator');

var serveur = http.Server(app);
serveur.listen(8080, function(){});

let database = JSON.parse(fs.readFileSync('pokedex.json'));
let accounts = JSON.parse(fs.readFileSync('public/accounts.json'));


app.get('/types', function (req, res) {
    var typelist = [];
    for (let i = 0; i < database.types.length; i++)
    {
        typelist.push(database.types[i]);
    }
    res.send(typelist);
});

app.get('/pokemons/all', function (req, res){
    var pokelist = [];
    for (let i = 0; i < database.pokemons.length; i++)
    {
        pokelist.push(database.pokemons[i]);
    }
    res.send(pokelist);
})

app.get('/pokemons/:type', function (req, res) {
    var pokelist = [];
    for (let i = 0; i < database.pokemons.length; i++)
    {
        for (let j = 0; j < database.pokemons[i].types.length; j++)
        {
            if (database.pokemons[i].types[j].nom.toLowerCase() == req.params.type.toLowerCase()) {
                pokelist.push(database.pokemons[i]);
            }
        }
    }
    res.send(pokelist);
});

app.get('/pokemons/nom/:type', function (req, res) {
    var pokelist = [];
    for (let i = 0; i < database.pokemons.length; i++)
    {
        for (let j = 0; j < database.pokemons[i].types.length; j++)
        {
            if (database.pokemons[i].types[j].nom.toLowerCase() == req.params.type)
            pokelist.push(database.pokemons[i].nom);
        }
    }
    res.send(pokelist);
});

// Route to Homepage
app.get('/index', (req, res) => {
    res.sendFile(__dirname + '/public/index.html');
});

app.get('/login/:username&:password', (req, res) => {
    let valid = -1;
    let username = req.params.username;
    let password = req.params.password;

    for (let i = 1; i < accounts.length; i++)
        if (username == accounts[i]["username"] && password == accounts[i]["password"])
            valid = i;
    if (valid != -1)
        res.send(accounts[valid]);
    else {
        if (username != "") {
            newAcc = accounts[0];
            newAcc.username = username;
            newAcc.password = password;
            accounts.push(newAcc);
            fs.writeFileSync("./public/accounts.json", JSON.stringify(accounts));
            accounts = JSON.parse(fs.readFileSync('public/accounts.json'));
            res.send(accounts[accounts.length-1]);
        }
    }
});

app.get('/score/:username', (req, res) => {
    let username = req.params.username;
    let valid = -1;
    for (let i = 1; i < accounts.length; i++)
        if (username == accounts[i]["username"])
            valid = i;
    accounts[valid].score++;
    fs.writeFileSync("./public/accounts.json", JSON.stringify(accounts));
    accounts = JSON.parse(fs.readFileSync('public/accounts.json'));
    res.send(accounts[valid]);
});

app.get('/img/:username&:img', (req, res) => {
    let username = req.params.username;
    let img = "./images/"+req.params.img;
    let valid = -1;
    for (let i = 1; i < accounts.length; i++)
        if (username == accounts[i]["username"])
            valid = i;
    console.log(img);
    accounts[valid].img = img;
    fs.writeFileSync("./public/accounts.json", JSON.stringify(accounts));
    accounts = JSON.parse(fs.readFileSync('public/accounts.json'));
    res.send(accounts[valid]);
});